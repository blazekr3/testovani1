package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainPage {
  private WebDriver driver;

  private By loginButton = By.id("login_btn");
  private By searchField = By.id("game");
  private By searchButton = By.id("search_button");

  public MainPage(WebDriver driver) {
    this.driver = driver;
  }

  public void clickLoginButton() {
    WebElement loginBtn = driver.findElement(loginButton);
    loginBtn.click();
  }
  public void clickSearchBar() {
    WebElement searchBar = driver.findElement(searchField);
    searchBar.click();
  }
  public void searchForProduct(String productName) {
    clickSearchBar();
    WebElement searchFieldElement = driver.findElement(searchField);
    WebElement searchButtonElement = driver.findElement(searchButton);
    searchFieldElement.sendKeys(productName);
    searchButtonElement.click();
  }
  public void clickOnProduct(String productName) {
    WebElement productLink = driver.findElement(By.xpath("//a[contains(text(), '" + productName + "')]"));
    productLink.click();
  }

}

