package cz.cvut.fel.ts1;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ProductPage {
  private WebDriver driver;

  private By addToCartButton = By.id("btn-buy");
  private By addToWhislistButton = By.id("wishlist_popup");
  private By confirmButton = By.id("confirm-buy");
  private By productHeading = By.cssSelector(".product_heading");

  public ProductPage(WebDriver driver) {
    this.driver = driver;
  }

  public void addToCart() {
    WebElement addToCartBtn = driver.findElement(addToCartButton);
    addToCartBtn.click();
  }

  public void addToWhislist() {
    WebElement addToWhislistBtn = driver.findElement(addToWhislistButton);
    addToWhislistBtn.click();
    try{
      Thread.sleep(1000);
    } catch (Exception e) {
      e.printStackTrace();
    }
    WebElement ListBtn = driver.findElement(By.cssSelector(".check-box"));
    ListBtn.click();
    WebElement submitBtn = driver.findElement(By.xpath("//button[text()='Uložit']"));
    submitBtn.click();
  }

  public void checkWhislist() {
    WebElement WhishListBtn = driver.findElement(By.id("wishlist_button"));
    WhishListBtn.click();
    try{
      Thread.sleep(1000);
    } catch (Exception e) {
      e.printStackTrace();
    }
    WebElement listCheckBtn = driver.findElement(By.xpath("//a[@title='Můj seznam']"));
    listCheckBtn.click();
  }

  public String getProductName() {
    WebElement productName = driver.findElement(productHeading);
    return productName.getText();
  }

  public void finishOrder() {
    try{
      Thread.sleep(1000);
    } catch (Exception e) {
      e.printStackTrace();
    }
    WebElement finishOrderBtn = driver.findElement(confirmButton);
    finishOrderBtn.click();
  }
}
