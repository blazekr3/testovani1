package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CartPage {
  private WebDriver driver;
  private WebDriverWait wait;

  private By cartItems = By.id("main_form");
  private By cartEmpty = By.xpath("//div[text()='Košík je zatím prázdný. Pro splnění mise jej musíte naplnit.']");

  private By removeFromCartButton = By.cssSelector(".btn_remove");

  public CartPage(WebDriver driver) {

    this.driver = driver;
    this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
  }

  public boolean isCartDisplayed() {
    WebElement cart = driver.findElement(cartItems);
    return cart.isDisplayed();
  }

  public void removeFromCart() {
    WebElement removeButton = driver.findElement(removeFromCartButton);
    removeButton.click();
  }

  public boolean isCartEmpty() {
    try {
      wait.until(ExpectedConditions.visibilityOfElementLocated(cartEmpty));
      return true;
    } catch (Exception e) {
      return false;
    }
  }
}
