package cz.cvut.fel.ts1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SearchResultsPage {
  private WebDriver driver;
  private WebDriverWait wait;

  private By sortByDropdown = By.id("filter-select");
  private By priceOption = By.xpath("//option[@value='price_asc']");
  private By productPrices = By.xpath("//span[contains(@class,'price')]");

  public SearchResultsPage(WebDriver driver) {
    this.driver = driver;
    this.wait = new WebDriverWait(driver, Duration.ofSeconds(2));
  }

  public void sortByPrice() {
    WebElement dropdown = wait.until(ExpectedConditions.elementToBeClickable(sortByDropdown));
    dropdown.click();
    WebElement priceOptionElement = wait.until(ExpectedConditions.elementToBeClickable(priceOption));
    priceOptionElement.click();
    try{
      Thread.sleep(1000);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public List<Double> getProductPrices() {
    wait.until(ExpectedConditions.presenceOfElementLocated(productPrices));
    List<WebElement> priceElements = driver.findElements(productPrices);

    return priceElements.stream()
            .limit(10)
            .map(WebElement::getText)
            .map(priceText -> priceText.replace(" Kč", "").replace(" ", "").replace(",", "."))
            .filter(priceText -> !priceText.isEmpty())
            .map(Double::parseDouble)
            .collect(Collectors.toList());
  }

}

