package cz.cvut.fel.ts1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SortByPriceTest {

  private WebDriver driver;
  private MainPage mainPage;
  private SearchResultsPage searchResultsPage;

  @BeforeEach
  public void setUp() {
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    mainPage = new MainPage(driver);
    searchResultsPage = new SearchResultsPage(driver);
  }

  @Test
  public void testSortByPrice() {
    driver.get("https://www.xzone.cz");

    mainPage.searchForProduct("Elden Ring");

    searchResultsPage.sortByPrice();

    List<Double> prices = searchResultsPage.getProductPrices();

    System.out.println("First 10 prices:");
    for (int i = 0; i < Math.min(prices.size(), 10); i++) {
      System.out.println(prices.get(i));
    }

    for (int i = 0; i < prices.size() - 1; i++) {
      assertTrue(prices.get(i) <= prices.get(i + 1), "Prices are not Descending");
    }


  }
}

