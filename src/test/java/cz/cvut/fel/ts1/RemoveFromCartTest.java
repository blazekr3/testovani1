package cz.cvut.fel.ts1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RemoveFromCartTest {

  private WebDriver driver;
  private MainPage mainPage;
  private WebDriverWait wait;
  private ProductPage productPage;
  private CartPage cartPage;

  @BeforeEach
  public void setUp() {
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    mainPage = new MainPage(driver);
    productPage = new ProductPage(driver);
    cartPage = new CartPage(driver);
  }

  @Test
  public void testRemoveFromCart() {
    driver.get("https://www.xzone.cz");

    mainPage.searchForProduct("Elden Ring");

    mainPage.clickOnProduct("Elden Ring");

    productPage.addToCart();

    String productName = productPage.getProductName();

    productPage.finishOrder();

    assertTrue(cartPage.isCartDisplayed(), "Product was not added to cart.");
    System.out.println(productName + " added to cart");

    cartPage.removeFromCart();

    assertTrue(cartPage.isCartEmpty(), "Cart is not Empty");

  }

}

