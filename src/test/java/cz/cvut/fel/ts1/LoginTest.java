package cz.cvut.fel.ts1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginTest {
  private WebDriver driver;
  private MainPage mainPage;

  @BeforeEach
  public void setUp() {
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    mainPage = new MainPage(driver);
  }

  @Test
  public void testUserLogin() {
    driver.get("https://www.xzone.cz");

    mainPage.clickLoginButton();

    WebElement emailField = driver.findElement(By.id("login"));
    WebElement passwordField = driver.findElement(By.name("heslo"));
    WebElement submitButton = driver.findElement(By.name("sent"));

    emailField.sendKeys("blazekr3@fel.cvut.cz");
    passwordField.sendKeys("XpGBbTge7Qb272d");
    submitButton.click();

    WebElement LoggedInButton = driver.findElement(By.id("logged_btn"));
    assertTrue(LoggedInButton.isDisplayed(), "Unsuccessful login.");
  }
}

