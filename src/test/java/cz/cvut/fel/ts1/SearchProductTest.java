package cz.cvut.fel.ts1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SearchProductTest {

  private WebDriver driver;
  private MainPage mainPage;

  @BeforeEach
  public void setUp() {
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    mainPage = new MainPage(driver);
  }

  @Test
  public void testSearchProduct() {

    driver.get("https://www.xzone.cz");

    mainPage.clickSearchBar();

    mainPage.searchForProduct("Elden Ring");

    WebElement gameProduct = driver.findElement(By.xpath("//a[contains(text(), 'Elden Ring')]"));
    assertTrue(gameProduct.isDisplayed(), "Elden Ring was not found.");

  }
}
