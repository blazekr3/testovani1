package cz.cvut.fel.ts1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ParameterizedTests {
  private WebDriver driver;
  private MainPage mainPage;
  private ProductPage productPage;
  private CartPage cartPage;
  private SearchResultsPage searchResultsPage;
  private WebDriverWait wait;

  @BeforeEach
  public void setUp() {
    driver = new ChromeDriver();
    driver.get("https://www.xzone.cz/");
    mainPage = new MainPage(driver);
    productPage = new ProductPage(driver);
    cartPage = new CartPage(driver);
    searchResultsPage = new SearchResultsPage(driver);
    wait = new WebDriverWait(driver, Duration.ofSeconds(2));
  }

  @AfterEach
  public void tearDown() {
    driver.quit();
  }

  @ParameterizedTest
  @CsvSource({
          "The Witcher 3, The Witcher 3",
          "DOOM Eternal, DOOM Eternal",
          "Red Dead Redemption 2, Red Dead Redemption 2",
  })
  public void testSearchAndAddToCart(String searchQuery, String expectedProductName) {
    mainPage.searchForProduct(searchQuery);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(), '" + expectedProductName + "')]")));
    mainPage.clickOnProduct(expectedProductName);
    wait.until(ExpectedConditions.elementToBeClickable(By.id("btn-buy")));
    productPage.addToCart();
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".product_heading")));
    assertTrue(productPage.getProductName().contains(expectedProductName));
  }

  @ParameterizedTest
  @CsvSource({
          "The Witcher 3",
          "DOOM Eternal",
          "Red Dead Redemption 2",
  })
  public void testRemoveFromCart(String productName) {
    mainPage.searchForProduct(productName);
    mainPage.clickOnProduct(productName);
    productPage.addToCart();
    productPage.finishOrder();
    cartPage.removeFromCart();
    assertTrue(cartPage.isCartEmpty());
  }

  @ParameterizedTest
  @CsvSource({
          "The Witcher 3",
          "DOOM Eternal",
          "Red Dead Redemption 2",
  })
  public void testSortByPrice(String productName) {
    mainPage.searchForProduct(productName);
    searchResultsPage.sortByPrice();
    List<Double> prices = searchResultsPage.getProductPrices();
    assertFalse(prices.isEmpty());
    for (int i = 0; i < prices.size() - 1; i++) {
      assertTrue(prices.get(i) <= prices.get(i + 1));
    }
  }

  @ParameterizedTest
  @CsvSource({
          "The Witcher 3",
          "DOOM Eternal",
          "Red Dead Redemption 2",
  })
  public void testSearchBarFunctionality(String productName) {
    mainPage.searchForProduct(productName);
    WebElement searchBar = driver.findElement(By.id("game"));
    assertEquals(productName, searchBar.getAttribute("value"));
  }

  @ParameterizedTest
  @CsvSource({
          "The Witcher 3, btn-buy",
          "DOOM Eternal, btn-buy",
          "Red Dead Redemption 2, btn-buy",
  })
  public void testAddToCartButtonPresence(String productName, String expectedButtonId) {
    mainPage.searchForProduct(productName);
    mainPage.clickOnProduct(productName);
    WebElement addToCartButton = driver.findElement(By.id(expectedButtonId));
    assertTrue(addToCartButton.isDisplayed());
  }
}

