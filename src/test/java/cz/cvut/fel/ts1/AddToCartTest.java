package cz.cvut.fel.ts1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddToCartTest {

  private WebDriver driver;
  private MainPage mainPage;
  private ProductPage productPage;
  private CartPage cartPage;

  @BeforeEach
  public void setUp() {
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    mainPage = new MainPage(driver);
    productPage = new ProductPage(driver);
    cartPage = new CartPage(driver);
  }

  @Test
  public void testAddProductToCart() {

    driver.get("https://www.xzone.cz");

    mainPage.searchForProduct("Elden Ring");

    mainPage.clickOnProduct("Elden Ring");

    productPage.addToCart();

    String productName = productPage.getProductName();

    productPage.finishOrder();

    assertTrue(cartPage.isCartDisplayed(), "Product was not added to cart.");
    System.out.println(productName + " added to cart");
  }
}

