package cz.cvut.fel.ts1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddToWhislistTest {

  private WebDriver driver;
  private MainPage mainPage;
  private ProductPage productPage;
  private CartPage cartPage;

  @BeforeEach
  public void setUp() {
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    mainPage = new MainPage(driver);
    productPage = new ProductPage(driver);
    cartPage = new CartPage(driver);
  }

  @Test
  public void testAddProductToCart() {

    driver.get("https://www.xzone.cz");

    mainPage.clickLoginButton();

    WebElement emailField = driver.findElement(By.id("login"));
    WebElement passwordField = driver.findElement(By.name("heslo"));
    WebElement submitButton = driver.findElement(By.name("sent"));

    emailField.sendKeys("blazekr3@fel.cvut.cz");
    passwordField.sendKeys("XpGBbTge7Qb272d");
    submitButton.click();

    mainPage.searchForProduct("Elden Ring");

    mainPage.clickOnProduct("Elden Ring");

    productPage.addToWhislist();

    productPage.checkWhislist();

    WebElement WhislistedName = driver.findElement(By.cssSelector(".objednavka_body"));

    assertTrue(WhislistedName.isDisplayed(), "The link with text 'Elden Ring' is not displayed");

  }
}
