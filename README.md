# Semestrální práce - Selenium testy - [Xzone.cz](https://www.xzone.cz/)

**Autor:**
Kryštof Blažek

## Obsah



## Popis práce

Tato práce se zaměřuje na testování webu [Xzone.cz](https://www.xzone.cz/). Jedná se o český eshop prodávájící výhradně videohry, herní příslušenství a další produkty související s herním průmyslem. Pro testování jsem použil [Selenium framework](https://www.selenium.dev/).

### Přehled částí aplikace

 Aplikace nabízí uživatelům následující hlavní funkce, zvírazněné jsou ty, které jsem testoval:

- **Přihlašování**
- Hlavní strana
- **Vyhledávání produktů**
- Filtrování produktů
- **Seřazení produktů podle ceny**
- **Přidávat položku do košíku**
- **Odebírat položku z košíku**
- **Přidávání položek do seznamu přání**
- Přidávání recenzí


### Priority částí aplikace

| Proces                | Podproces         | Požadavek                                           | Možné poškození                                | Vysvětlení poškození                                            | Část systému            | Pravděpodobnost selhání  | Vysvětlení pravděpodobnosti selhání                            | Třída rizika |
|-----------------------|-------------------|-----------------------------------------------------|------------------------------------------------|-----------------------------------------------------------------|-------------------------|--------------------------|----------------------------------------------------------------|--------------|
| Nakupování            | Přidání do košíku | Přidat produkt do košíku                            | H                                              | Uživatel nemůže pokračovat v nákupu                             | Nákupní košík           | Střední                  | Problémy mohou nastat při přidávání nových produktů do systému | A            |
| Vyhledávání           | Vyhledávání       | Vyhledat produkt podle klíčových slov               | L                                              | Uživatel nemůže najít požadovaný produkt                        | Vyhledávací modul       | Vysoká                   | Vyhledávací algoritmus je komplexní a citlivý na změny         | B            |
| Filtrování produktů   | Filtrování        | Aplikovat filtr na seznam produktů                  | L                                              | Uživatel je zahlcen nerelevantními produkty                     | Filtrační modul         | Střední                  | Filtry závisí na správném přiřazení atributů produktů          | B            |
| Hlídání ceny produktů | Nastavení hlídání | Umožnit uživateli nastavit hlídání ceny produktu    | M                                              | Uživatel nedostane upozornění o změně ceny                      | Upozornění a notifikace | Nízká                    | Mechanismus hlídání ceny je relativně jednoduchý               | C            |


### Test levels

### Test Levels pro Xzone.cz

|        Části systému        | Třída rizika | Vývojářské/unit testy | Systémové testy |  UAT   | Testy po spuštění v produkci |
|:---------------------------:|:------------:|:---------------------:|:---------------:|:------:|:----------------------------:|
|      **Funkcionalita**      |              |                       |                 |        |                              |
|         Nakupování          |      A       |        vysoká         |      vysoká     | vysoká |             ano              |
|        Nákupní košík        |      A       |        vysoká         |      vysoká     | vysoká |             ano              |
|        Přihlašování         |      A       |        vysoká         |      vysoká     | nízká  |             ano              |
|         Vyhledávání         |      B       |        střední        |      vysoká     | nízká  |             ano              |
|     Filtrování produktů     |      B       |        střední        |      vysoká     | nízká  |             ano              |
|     Hlídání ceny produktů   |      C       |         nízká         |      nízká      | nízká  |              ne              |
| **Uživatelská přívětivost** |              |                       |                 |        |                              |
|         Nakupování          |      A       |                       |     vysoká      | vysoká |             ano              |
|        Nákupní košík        |      A       |                       |     vysoká      | vysoká |             ano              |
|        Přihlašování         |      A       |                       |      střední    | nízká  |             ano              |
|         Vyhledávání         |      B       |                       |     vysoká      | nízká  |             ano              |
|     Filtrování produktů     |      B       |                       |     vysoká      | nízká  |             ano              |
|     Hlídání ceny produktů   |      C       |                       |      nízká      | nízká  |              ne              |


## Testovací scénáře

### Testy vstupů

#### Mezní podmínky EC

Mezní podmínky se vyskytují ve vyhledávání v celé webové aplikaci.
Intervalové proměnné se týkají především ceny produktu.
Nominální (kategorické) proměnné se vyskytují v podrobnějších filtrech. Tyto proměnné jsou obvykle reprezentovány zaškrtávacími políčky, kde se jedná jak o typ radio, tak typ checkbox.

#### Pairwise testing

Pro Nakupování produktů jsem zvolil testovací parametry následovně:

|        Parametr        | Hodnoty                               |
|:----------------------:|:-------------------------------------:|
|   Typ produktu         |   [Hra, Příslušenství, Merch]         |
|   Platba               |   [Debitní karta, PayPal, Dobírka]    |
|   Doprava              |   [Kurýr, Osobní odběr]               |

Technikou pairwise testování jsem získal takovou kombinaci parametrů:

| Typ produktu  | Platba         | Doprava     |
|---------------|----------------|-------------|
| Hra           | Debitní karta  | Kurýr       |
| Hra           | PayPal         | Osobní odběr|
| Hra           | Dobírka        | Kurýr       |
| Příslušenství | Debitní karta  | Osobní odběr|
| Příslušenství | PayPal         | Kurýr       |
| Příslušenství | Dobírka        | Osobní odběr|
| Merch         | Debitní karta  | Kurýr       |
| Merch         | PayPal         | Osobní odběr|
| Merch         | Dobírka        | Kurýr       |


### Testy průchodů

#### Procesní diagram

![procesní-diagram-1](Images/Procesní_diagram1.png)

![graph1](Images/Graph1.png)

![edges](Images/Sub-combinations-of-edges.png)

![situations](Images/Test-situations.png)

### Detailní testovací scénář

## Přidání Produktu do Košíku a Dokončení Objednávky

Testovací Kroky:
- 1. Přihlášení uživatele
- 2. Vyhledání produktu
- 3. Zobrazenídetailu produktu
- 4. Přidání produktu do košíku
- 5. Zobrazení košíku
- 6. Přechod k platbě
- 7. Výběr způsobu dopravy a platby
- 8. Zadání doručovacích údajů
- 9. Potvrzení objednávky
- 10. Zaplacení


